//
//  AppDelegate.m
//  Passgen
//
//  Created by Akasaka Ryuunosuke on 09/07/15.
//  Copyright (c) 2015 Akasaka Ryuunosuke. All rights reserved.
//

#import "AppDelegate.h"
#import "SFPasswordAssistantInspectorView.h"
#import "OBMenuBarWindow.h"
@interface AppDelegate () {
    NSView *passV;
}

@property (weak) IBOutlet SFPasswordAssistantInspectorController *pwass;
@property (weak) IBOutlet OBMenuBarWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    self.window.menuBarIcon = [NSImage imageNamed: @"menubar"];
    self.window.highlightedMenuBarIcon = [NSImage imageNamed:  @"menubar_invert"];
    self.window.hasMenuBarIcon = YES;
    
    self.window.isDetachable = YES;
    
    [_pwass showPasswordAssistantPanel:nil];
    passV = ((NSView*)[_pwass valueForKey:@"_passwordAssistantView"]);
    [self.window.contentView addSubview:passV];
    [passV setFrame:NSMakeRect(0, 0, passV.frame.size.width, passV.frame.size.height)];
    [self.window setFrame:NSMakeRect(self.window.frame.origin.x, self.window.frame.origin.y, passV.frame.size.width, passV.frame.size.height+20) display:true animate:true];
    self.window.title = ((NSWindow*)[_pwass valueForKey:@"_passwordAssistantPanel"]).title;
    
    [((NSWindow*)[_pwass valueForKey:@"_passwordAssistantPanel"]) close];
    
    self.window.attachedToMenuBar = YES;
    
    [self.window close];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
