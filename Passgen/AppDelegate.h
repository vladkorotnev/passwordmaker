//
//  AppDelegate.h
//  Passgen
//
//  Created by Akasaka Ryuunosuke on 09/07/15.
//  Copyright (c) 2015 Akasaka Ryuunosuke. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

